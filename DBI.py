import MySQLdb;


class db_interface:
    cursor = None
    db = None

    def __init__(self, db_name, username="root", password="root"):
        localhost = "127.0.0.1"
        self.db = MySQLdb.connect(host="127.0.0.1", user=username, passwd=password, db=db_name)
        self.cursor = self.db.cursor()

    def exec_sql(self, str_sql):
        if not str_sql:
            print 'Empty SQL Query!!'
            str_sql = "SHOW TABLES;"
        try:
            self.cursor.execute(str_sql)
            self.db.commit()
            print(cursor._last_executed)
        except:
            self.db.rollback()

    def close(self):
        self.db.close()


def get_dbi():
    db_name = "Weather_v1"
    dbi = db_interface(db_name)
    return dbi;
