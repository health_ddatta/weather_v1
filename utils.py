import datetime
import time
import math

def get_current_year():
        return int(time.strftime("%Y"))

def get_current_day():
        today = datetime.datetime.now()
        day = today.strftime('%j')
        return int(day)

def get_dims():
        return 181,360;

def get_years_least_week(year):
    return  datetime.date (year, 12, 28).isocalendar ()[ 1 ]