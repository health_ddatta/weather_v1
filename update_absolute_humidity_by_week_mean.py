from datetime import date
from multiprocessing import Process
import numpy as np
import constants
import mongoDBI
import AH_calc
import utils




def set_AH(year):
    dbi = mongoDBI.mongoDBI (constants.db_name)
    temp_table = 'temp'
    rel_hum_table = 'rel_hum'
    last_week = utils.get_years_least_week(year)
    table = 'abs_hum'
    for week in range (1, last_week + 1, 1):
        ah_arr = np.zeros (utils.get_dims ())
        id = str (year) + '_' + str (week)

        temp_arr = dbi.find (temp_table, constants.year_week_label, id, constants.label_value)
        rh_arr = dbi.find (rel_hum_table, constants.year_week_label, id, constants.label_value)
        if temp_arr is None or rh_arr is None:
            continue;

        for i in range (temp_arr.shape[ 0 ]):
            for j in range (temp_arr.shape[ 1 ]):
                ah_arr[ i, j ] = AH_calc.calc_ah (temp_arr[ i, j ], rh_arr[ i, j ])

        # Write to db

        key_label = constants.year_week_label
        key_contents = id
        value_label = constants.label_value
        value_contents = ah_arr
        dbi.insert_obj (table, key_label, key_contents, value_label, value_contents)
    return


def set_ah_by_year():
    cur_year = utils.get_current_year ()
    years = range (constants.gdas_start_year, cur_year + 1)
    process_pool = [ ]

    for year in years:
        p = Process (target=set_AH, args=(year,))
        process_pool.append (p);

    for p in process_pool:
        p.start ()
        # pass
    for p in process_pool:
        p.join ()
        # pass
    return;


# dAH = AH - avg(AH)
# avg(AH) is historical average of daily AH values for a location
def set_dAH():
    dbi = mongoDBI.mongoDBI (constants.db_name)
    sum_history = np.zeros (utils.get_dims ())
    cur_year = utils.get_current_year ()
    years = range (constants.gdas_start_year, cur_year + 1)
    table = 'abs_hum'
    count = 0
    for year in years:
        last_week = utils.get_years_least_week (year)
        for week in range (1, last_week + 1):
            id = str (year) + '_' + str (week)
            tmp_arr = dbi.find (table, constants.year_week_label, id, constants.label_value)

            if tmp_arr is None:
                continue
            sum_history = sum_history + tmp_arr
            count += 1

    avg_ah = sum_history / count

    dAH_table = constants.dAH_table
    for year in years:
        last_week = utils.get_years_least_week (year)
        for week in range (1, last_week + 1):
            id = str (year) + '_' + str (week)
            tmp_arr = dbi.find (table, constants.year_week_label, id, constants.label_value)
            if tmp_arr is None:
                continue

            dah_arr = tmp_arr - avg_ah
            dbi.insert_obj (dAH_table, constants.year_week_label, id, constants.label_value, dah_arr)

    return;

# ---- Program execution start point ----#

# set_ah_by_year ()
# set_dAH()
