'''
# Reference : https://www.aqua-calc.com/calculate/humidity
# AH : kg/m^3 = Pw / (Rw * T)
# T is ambient temperature in Kelvin,
# Rw is specific gas constant for water vapor and it is equal to 461.5.
# Water Vapor Pressure Pw = RH * Pws /100
# where  Pw is water vapor pressure,  RH is relative humidity (%),  Pws is saturated water vapor pressure.
# Pws = 101325 Pa
'''


#Input :
# Temp in K | float
# RH as % | float
def calc_ah(temp,rh):
    p_w = rh * float(101325) /100
    ah = p_w/float(461.5*temp)
    return ah
