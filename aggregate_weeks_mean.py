from multiprocessing import Process
import mongoDBI
import constants
from datetime import datetime, timedelta ,date
import load_db
import os
import utils
import update_absolute_humidity as update_ah



def get_week_number(yyyy, mm, dd):
    date = datetime.date(yyyy, mm, dd)
    week = date.isocalendar()[1]
    print date.isocalendar()
    return week,



def process_year_by_week(year):

    dbi = mongoDBI.mongoDBI(constants.db_name_week_mean)
    # determine the 1st date of the
    dt = date(year, 1, 1)
    wk = dt.isocalendar()[1]
    wk_day = dt.isocalendar()[2]
    first_date = dt


    if wk > 1:
        # previous ISO year includes 1 st Jan.
        diff = 7 - wk_day + 1
        first_date = dt + timedelta(days=diff)
    elif wk ==1 and wk_day > 1 :
        # include days from previous gregorian calendar year
        diff = wk_day - 1
        first_date = dt - timedelta(days=diff)



    # Because of format of ISO calendar, as the first ISO week is one with at least 4 days in the calendar year
    # 28th December always lies in last week
    last_week = date(year, 12, 28).isocalendar()[1]
    prev = None

    os.chdir (constants.data_dir)
    os.chdir (constants.gdas_data_dir)
    for week in range(1,last_week+1,1):

        if week == 1 :
            cur_wk_start = first_date
        else :
            cur_wk_start = prev + timedelta(days=1)
        cur_wk_end = cur_wk_start + timedelta(days=6)

        start = cur_wk_start.strftime('%Y-%m-%d')
        load_db.load_by_week( start, year,  week)
        prev = cur_wk_end
    os.chdir ("../")
    os.chdir ("../")


def process_all_years_week_mean():

    cur_year = utils.get_current_year ()
    years = range (constants.gdas_start_year, cur_year + 1)
    process_pool = [ ]


    for year in years:
        p = Process (target=process_year_by_week, args=(year,))
        process_pool.append (p);

    for p in process_pool:
        p.start ()
        #pass
    for p in process_pool:
        p.join ()
        #pass
	
    update_ah.set_ah_by_year ()
    update_ah.set_dAH()


    return;
